package CaveService.api;

import CaveService.mongo.RoomReadStorage;
import CaveService.mongo.RoomWriteStorage;
import CaveService.model.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.time.OffsetDateTime;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-04-14T15:40:41.753Z")

@Controller
public class Room implements RoomApi {

    private static final Logger logger = LoggerFactory.getLogger(Room.class);
    private final ObjectMapper objectMapper;
    private final HttpServletRequest request;
    private final RoomWriteStorage roomWriteStorage;
    private final RoomReadStorage roomReadStorage;

    @org.springframework.beans.factory.annotation.Autowired
    public Room(ObjectMapper objectMapper, HttpServletRequest request, RoomWriteStorage roomWriteStorage, RoomReadStorage roomReadStorage) {
        this.objectMapper = objectMapper;
        this.request = request;
        this.roomWriteStorage = roomWriteStorage;
        this.roomReadStorage = roomReadStorage;
    }

    public ResponseEntity<RoomRecord> getRoom(@ApiParam(value = "Position of the room in the (x,y,z) format.",required=true) @PathVariable("position") String position) {
        String accept = request.getHeader("Accept");
        logger.info("method=getRoom, position='{}'", position);
        return roomReadStorage.getRoom(position);
    }
    public ResponseEntity<Void> createRoom(@ApiParam(value = "Position of the room in the (x,y,z) format.",required=true) @PathVariable("position") String position,
                                           @ApiParam(value = "The room to create" ,required=true )  @Valid @RequestBody RoomRecord roomRecord) {
        String accept = request.getHeader("Accept");
        logger.info("method=createRoom, position='{}'", position);
        roomRecord.setId(position);
        roomRecord.setCreationTimeISO8601(OffsetDateTime.now().toString());
        return roomWriteStorage.createRoom(position, roomRecord);
    }

    public ResponseEntity<Void> updateRoomDescription(@ApiParam(value = "Position of the room in the (x,y,z) format.",required=true) @PathVariable("position") String position,
                                                      @ApiParam(value = "Updated description of the room", required = true) @PathVariable("description") String description) {
        String accept = request.getHeader("Accept");
        //logger.info("method=updateRoom, position='{}', description='{}'", position, description);
        logger.info("method=createRoom, position='{}'", position);
        return roomWriteStorage.updateRoomDescription(position, description);
    }

    public ResponseEntity<Void> updateRoomColor(@ApiParam(value = "Position of the room in the (x,y,z) format.",required=true) @PathVariable("position") String position,
                                                @ApiParam(value = "Updated color of the room", required = true) @PathVariable("color") String color) {
        String accept = request.getHeader("Accept");
        //logger.info("method=updateRoom, position='{}', color='{}'", position, color);
        logger.info("method=createRoom, position='{}'", position);
        return roomWriteStorage.updateRoomColor(position, color);
    }

    public ResponseEntity<Void> undoRoomEvent(@ApiParam(value = "Position of the room in the (x,y,z) format.",required=true) @PathVariable("position") String position) {
        String accept = request.getHeader("Accept");
        logger.info("method=createRoom, position='{}'", position);
        return roomWriteStorage.undoRoomEvent(position);
    }

    public ResponseEntity<Exits> getExits(@ApiParam(value = "Position of the room in the (x,y,z) format.",required=true) @PathVariable("position") String position) {
        logger.info("method=getExits, position='{}'", position);
        return roomWriteStorage.getExits(position);
    }
}
