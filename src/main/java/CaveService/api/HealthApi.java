package CaveService.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import CaveService.model.HealthRecord;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-05-17T15:46:14.849Z")

@Api(value = "health", description = "the health API")
@RequestMapping(value = "/")
public interface HealthApi {

    @ApiOperation(value = "Get service health information", nickname = "healthGet", notes = "Get health informations about the CaveService itself.", response = HealthRecord.class, tags={ "health", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Healt informations sucessfully retrieved", response = HealthRecord.class),
            @ApiResponse(code = 400, message = "Invalid request") })
    @RequestMapping(value = "/health", method = RequestMethod.GET, produces = {"application/json"})
    ResponseEntity<HealthRecord> getHealth();

}