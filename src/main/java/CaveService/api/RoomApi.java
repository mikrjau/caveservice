package CaveService.api;

import CaveService.model.*;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-04-14T15:40:41.753Z")

@Api(value = "room")
@RequestMapping(value = "/")
public interface RoomApi {

  /** Unique player ID of Will Crowther */
  public static final String WILL_CROWTHER_ID = "0";

  @ApiOperation(value = "Get a room", nickname = "roomPositionGet", notes = "Get a room at the given position.", response = RoomRecord.class, tags = {})
  @ApiResponses(value = {
          @ApiResponse(code = 200, message = "Room sucessfully retrieved", response = RoomRecord.class),
          @ApiResponse(code = 400, message = "Invalid request"),
          @ApiResponse(code = 404, message = "Room not found")})
  @RequestMapping(value = "/room/{position}", method = RequestMethod.GET, produces = {"application/json"})
  default ResponseEntity<RoomRecord> getRoom(@ApiParam(value = "Position of the room in the (x,y,z) format.", required = true) @PathVariable("position") String position){
    return new ResponseEntity<RoomRecord>(HttpStatus.NOT_IMPLEMENTED);
  }

  @ApiOperation(value = "Create a new room event", nickname = "roomPositionPost", notes = "Create a new room at the given position.", tags = {})
  @ApiResponses(value = {
          @ApiResponse(code = 201, message = "Room successfully created"),
          @ApiResponse(code = 400, message = "Invalid request"),
          @ApiResponse(code = 409, message = "Room already exists")})
  @RequestMapping(value = "/room/{position}", method = RequestMethod.POST, produces = {"application/json"})
  @ResponseStatus(HttpStatus.CREATED)
  default ResponseEntity<Void> createRoom(@ApiParam(value = "Position of the room in the (x,y,z) format.", required = true) @PathVariable("position") String position,
                                          @ApiParam(value = "The room to create", required = true) @Valid @RequestBody RoomRecord roomRecord) {
    return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
  }

  @ApiOperation(value = "Update description in a room event", nickname = "roomDescriptionPost", notes = "Update description in a room at the given position.", tags = {})
  @ApiResponses(value = {
          @ApiResponse(code = 201, message = "Upate room describtion event successfully created"),
          @ApiResponse(code = 400, message = "Invalid request"),
          @ApiResponse(code = 404, message = "Room not found")})
  @RequestMapping(value = "/room/{position}/description/{description}", method = RequestMethod.POST, produces = {"application/json"})
  @ResponseStatus(HttpStatus.CREATED)
  default ResponseEntity<Void> updateRoomDescription(@ApiParam(value = "Position of the room in the (x,y,z) format.", required = true) @PathVariable("position") String position,
                                                     @ApiParam(value = "Updated description of the room", required = true) @PathVariable("description") String description) {
    return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
  }

  @ApiOperation(value = "Update color in a room event", nickname = "roomColorPost", notes = "Update color in a room at the given position.", tags = {})
  @ApiResponses(value = {
          @ApiResponse(code = 201, message = "Upate room color event successfully created"),
          @ApiResponse(code = 400, message = "Invalid request"),
          @ApiResponse(code = 404, message = "Room not found")})
  @RequestMapping(value = "/room/{position}/color/{color}", method = RequestMethod.POST, produces = {"application/json"})
  @ResponseStatus(HttpStatus.CREATED)
  default ResponseEntity<Void> updateRoomColor(@ApiParam(value = "Position of the room in the (x,y,z) format.", required = true) @PathVariable("position") String position,
                                               @ApiParam(value = "Updated color of the room", required = true) @PathVariable("color") String color) {
    return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
  }

  @ApiOperation(value = "Undo the last room event", nickname = "roomEventUndoPost", notes = "Undo the last room at the given position.", tags = {})
  @ApiResponses(value = {
          @ApiResponse(code = 201, message = "Undo the last room event successfully performed"),
          @ApiResponse(code = 400, message = "Invalid request"),
          @ApiResponse(code = 404, message = "Room not found")})
  @RequestMapping(value = "/room/{position}/undo", method = RequestMethod.POST, produces = {"application/json"})
  @ResponseStatus(HttpStatus.CREATED)
  default ResponseEntity<Void> undoRoomEvent(@ApiParam(value = "Position of the room in the (x,y,z) format.", required = true) @PathVariable("position") String position) {
    return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
  }

  @ApiOperation(value = "Get exits for room", nickname = "roomPositionExitsGet", notes = "Get exits from room at the given position.", response = Exits.class, tags = {})
  @ApiResponses(value = {
          @ApiResponse(code = 200, message = "Exists", response = Exits.class),
          @ApiResponse(code = 400, message = "Invalid request"),
          @ApiResponse(code = 404, message = "Room not found")})
  @RequestMapping(value = "/room/{position}/exits", method = RequestMethod.GET, produces = {"application/json"})
  default ResponseEntity<Exits> getExits(@ApiParam(value = "Position of the room in the (x,y,z) format.", required = true) @PathVariable("position") String position) {
    return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
  }
}
