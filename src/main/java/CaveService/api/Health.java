package CaveService.api;

import CaveService.health.HealthCheck;
import CaveService.model.HealthRecord;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpServletRequest;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-05-17T15:46:14.849Z")

@Controller
public class Health implements HealthApi {

    private static final Logger log = LoggerFactory.getLogger(Health.class);
    private final ObjectMapper objectMapper;
    private final HttpServletRequest request;
    private HealthCheck healthCheck;

    @org.springframework.beans.factory.annotation.Autowired
    public Health(ObjectMapper objectMapper, HttpServletRequest request, HealthCheck healthCheck) {
        this.objectMapper = objectMapper;
        this.request = request;
        this.healthCheck = healthCheck;
    }

    public ResponseEntity<HealthRecord> getHealth() {
        return healthCheck.geHealth();
    }

}
