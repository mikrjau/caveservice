package CaveService.configuration;

import CaveService.doubles.FakeRoomStorage;
import CaveService.mongo.RoomReadStorage;
import CaveService.mongo.RoomWriteStorage;
import CaveService.health.HealthCheck;
import CaveService.health.ServiceHealthCheck;
import CaveService.messaging.Projector;
import CaveService.messaging.Publisher;
import CaveService.messaging.Subscriber;
import CaveService.mongo.MongoReadStorage;
import CaveService.mongo.MongoWriteStorage;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeoutException;

@Configuration
public class ServiceConfig {
  Publisher publisher;
  Subscriber subscriber;
  Projector projector;
  Map<String, String> env = System.getenv();
  String storageType = env.get("STORAGE_TYPE");
  String mongoHost = env.get("STORAGE_HOST") != null ? env.get("STORAGE_HOST") : "localhost";
  Integer mongoPort = env.get("STORAGE_PORT") != null ? Integer.parseInt(env.get("STORAGE_PORT")) : 27017;
  String rabbitHost = env.get("RABBIT_HOST") != null ? env.get("RABBIT_HOST") : "localhost";
  Integer rabbitPort = env.get("RABBIT_PORT") != null ? Integer.parseInt(env.get("RABBIT_PORT")) : 5672;


  @Bean
  public RoomWriteStorage roomWriteStorage() throws IOException, TimeoutException {
    if(storageType != null && storageType.matches("db")){
      return setupMongoWriteStorage();
    } else {
      return setupFakeStorage();
    }
  }

  @Bean
  public RoomReadStorage roomReadStorage() throws IOException, TimeoutException {
    if(storageType != null && storageType.matches("db")){
      return setupMongoReadStorage();
    } else {
      return setupFakeStorage();
    }
  }

  private MongoWriteStorage setupMongoWriteStorage() throws IOException, TimeoutException {
    publisher = new Publisher(rabbitHost, rabbitPort);
    projector = new Projector(mongoHost, mongoPort);
    subscriber = new Subscriber(projector, rabbitHost, rabbitPort);

    System.out.println("Using MongoWriteStorage.");
    MongoWriteStorage writeStorage = new MongoWriteStorage(mongoHost, mongoPort, publisher);

    writeStorage.init();
    subscriber.init();
    return writeStorage;
  }

  private RoomReadStorage setupMongoReadStorage() {
    System.out.println("Using MongoReadStorage.");
    MongoReadStorage readStorage = new MongoReadStorage(mongoHost, mongoPort);
    readStorage.init();
    return readStorage;
  }

  private FakeRoomStorage setupFakeStorage() {
    System.out.println("Using in-memory storage FakeRoomStorage.");
    FakeRoomStorage roomStorage = new FakeRoomStorage();
    roomStorage.init();
    return roomStorage;
  }

  @Bean
  public HealthCheck healthCheck() {
    return new ServiceHealthCheck(mongoHost, mongoPort);
  }
}

