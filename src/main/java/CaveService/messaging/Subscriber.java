package CaveService.messaging;

import CaveService.model.Events.*;
import com.google.gson.Gson;
import com.rabbitmq.client.*;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

public class Subscriber {
    private final static String QUEUE_NAME = "events";
    private final Gson gson;
    private Projector projector;
    private final String host;
    private final Integer port;

    public Subscriber(Projector projector, String host, Integer port) {
        gson = new Gson();
        this.projector = projector;
        this.host = host;
        this.port = port;
    }

    public void init() throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(host);
        factory.setPort(port);
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);

        DeliverCallback deliverCallback = (consumerTag, delivery) -> processEvent(delivery);
        channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> { });
    }

    private void processEvent(Delivery delivery) {
        try {
            String jsonString = new String(delivery.getBody(), StandardCharsets.UTF_8);
            System.out.println("RABBIT RECEIVED: '" + jsonString + "'");

            EventType eventType = gson.fromJson(jsonString, EventType.class);
            switch(eventType.type) {
                case "createRoom":
                    CreateRoomEventDTO createRoomEventDTO = gson.fromJson(jsonString, CreateRoomEventDTO.class);
                    projector.apply(createRoomEventDTO);
                    break;
                case "updateColor":
                    UpdateRoomColorEventDTO updateRoomColorEventDTO = gson.fromJson(jsonString, UpdateRoomColorEventDTO.class);
                    projector.apply(updateRoomColorEventDTO);
                    break;
                case "updateDescription":
                    UpdateRoomDescriptionEventDTO updateRoomDescriptionEventDTO = gson.fromJson(jsonString, UpdateRoomDescriptionEventDTO.class);
                    projector.apply(updateRoomDescriptionEventDTO);
                    break;
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}
