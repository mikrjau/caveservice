package CaveService.messaging;

import CaveService.model.Events.EventDTO;
import com.google.gson.Gson;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Publisher {
    private final Gson gson;
    private final static String QUEUE_NAME = "events";
    private final Channel channel;

    public Publisher(String host, int port) throws IOException, TimeoutException {
        gson = new Gson();
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(host);
        factory.setPort(port);
        Connection connection = factory.newConnection();
        channel = connection.createChannel();
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
    }

    public void send(EventDTO event) throws IOException {
        channel.basicPublish("", QUEUE_NAME, null, gson.toJson(event).getBytes());
        System.out.println(" [x] Sent '" + event + "'");
    }
}
