package CaveService.messaging;

import CaveService.model.Events.CreateRoomEventDTO;
import CaveService.model.Events.EventDTO;
import CaveService.model.Events.UpdateRoomColorEventDTO;
import CaveService.model.Events.UpdateRoomDescriptionEventDTO;
import com.mongodb.MongoClientSettings;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.Indexes;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.mongodb.client.model.Filters.eq;

public class Projector {

    private final MongoCollection<Document> roomsCollection;

    public Projector(String host, Integer port) {
        List<ServerAddress> connections = Arrays.stream(host.split(",")).map(s -> new ServerAddress(s, port))
                .collect(Collectors.toList());
        System.out.println("Projector hosts list: " + connections.toString());
        MongoClient mongoClient = MongoClients.create(
                MongoClientSettings.builder()
                        .applyToClusterSettings(builder ->
                                builder.hosts(connections))
                        .build());
        MongoDatabase database = mongoClient.getDatabase("cave");
        roomsCollection = database.getCollection("rooms");
    }

    public void apply(CreateRoomEventDTO event) {
        Document document = createRoomEventToDocument(event);
        roomsCollection.insertOne(document);
    }

    public void apply(UpdateRoomColorEventDTO event) {
        UpdateDocument(event.getPosition(), "color", event.getColor());
    }

    public void apply(UpdateRoomDescriptionEventDTO event) {
        UpdateDocument(event.getPosition(), "description", event.getDescription());
    }

    private void UpdateDocument(String position, String field, String value) {
        Bson filter = eq("position", position);
        Document setDocument = new Document("$set", new Document(field, value));
        roomsCollection.updateOne(filter, setDocument);
    }

    private Document createRoomEventToDocument(CreateRoomEventDTO createRoomEvent){
        return new Document()
                .append("position", createRoomEvent.getPosition())
                .append("timestamp", createRoomEvent.getTimestamp())
                .append("creatorId", createRoomEvent.getCreatorId())
                .append("color", createRoomEvent.getColor())
                .append("description", createRoomEvent.getDescription());
    }
}
