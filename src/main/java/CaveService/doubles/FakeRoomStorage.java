package CaveService.doubles;

import CaveService.model.*;
import CaveService.mongo.RoomReadStorage;
import CaveService.mongo.RoomWriteStorage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class FakeRoomStorage implements RoomWriteStorage, RoomReadStorage {
    private Map<String, RoomRecord> roomRecordMap;

    public FakeRoomStorage(){
        roomRecordMap = new HashMap<String, RoomRecord>();
    }

    @Override
    public void init() {
        // Initialize the default room layout
        this.createRoom(new Point3(0, 0, 0).getPositionString(), new RoomRecord("(0,0,0)",
                "You are standing at the end of a road before a small brick building.",
                "black",
                WILL_CROWTHER_ID));
        this.createRoom(new Point3(0, 1, 0).getPositionString(), new RoomRecord("(0,1,0)",
                "You are in open forest, with a deep valley to one side.", "white", WILL_CROWTHER_ID));
        this.createRoom(new Point3(1, 0, 0).getPositionString(), new RoomRecord("(1,0,0)",
                "You are inside a building, a well house for a large spring.", "red", WILL_CROWTHER_ID));
        this.createRoom(new Point3(-1, 0, 0).getPositionString(), new RoomRecord("(-1,0,0)",
                "You have walked up a hill, still in the forest.", "yellow", WILL_CROWTHER_ID));
        this.createRoom(new Point3(0, 0, 1).getPositionString(), new RoomRecord("(0,0,1)",
                "You are in the top of a tall tree, at the end of a road.", "green", WILL_CROWTHER_ID));
    }

    @Override
    public ResponseEntity<RoomRecord> getRoom(String positionString) {
        RoomRecord roomRecord = null;

        if (!roomRecordMap.containsKey(positionString) ) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        } else {
            try {
                roomRecord = roomRecordMap.get(positionString);
            } catch(Exception ex) {
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }
        }

        return new ResponseEntity<RoomRecord>(roomRecord, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> createRoom(String positionString, RoomRecord newRoom) {
        // if there is already a room, return false
        if (roomRecordMap.containsKey(positionString) ) {
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }
        // Simulate classic DB behaviour: timestamp record and
        // assign unique id
        RoomRecord recordInDB = new RoomRecord(newRoom);

        String now = OffsetDateTime.now().toString();
        recordInDB.setCreationTimeISO8601(now);
        recordInDB.setId(UUID.randomUUID().toString());

        try {
            roomRecordMap.put(positionString, recordInDB);
        } catch(Exception ex){
            return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Void> updateRoomDescription(String positionString, String description) {
        RoomRecord presentRecord = null;

        // if room does not exist, return 404 NOT FOUND
        if (!roomRecordMap.containsKey(positionString) ) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        } else {
            try {
                presentRecord = roomRecordMap.get(positionString);

            } catch(Exception ex) {
                return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
            }
        }

        presentRecord.setDescription(description);

        roomRecordMap.put(positionString, presentRecord);

        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Void> updateRoomColor(String positionString, String color) {
        RoomRecord presentRecord = null;

        // if room does not exist, return 404 NOT FOUND
        if (!roomRecordMap.containsKey(positionString) ) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        } else {
            try {
                presentRecord = roomRecordMap.get(positionString);

            } catch(Exception ex) {
                return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
            }
        }

        presentRecord.setColor(color);

        roomRecordMap.put(positionString, presentRecord);

        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Void> undoRoomEvent(String positionString) {
        // if room does not exist, return 404 NOT FOUND
        if (!roomRecordMap.containsKey(positionString) ) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }

        //ToDo: delete last event in event table
        /*
        try {
            roomRecordMap.put(positionString, recordInDB);
        } catch(Exception ex){
            return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
        }
         */

        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Exits> getExits(String positionString){
        Exits listOfExits = new Exits();

        if (!roomRecordMap.containsKey(positionString)){
            return new ResponseEntity<Exits>(HttpStatus.NOT_FOUND);
        }

        try {
            Point3 pZero = Point3.parseString(positionString);
            Point3 p;
            for (ExitType d : ExitType.values()) {
                p = new Point3(pZero.x(), pZero.y(), pZero.z());
                p.translate(d);
                String position = p.getPositionString();
                if (roomRecordMap.containsKey(position)) {
                    listOfExits.add(d);
                }
            }
        } catch(Exception ex) {
            return new ResponseEntity<Exits>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<Exits>(listOfExits, HttpStatus.OK);
    }
}
