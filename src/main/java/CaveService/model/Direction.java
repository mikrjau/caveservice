package CaveService.model;

/** The directions in the grid of the cave.
 *
 * @author Henrik Baerbak Christensen, Aarhus University.
 *
 */
public enum Direction {
    NORTH, SOUTH, EAST, WEST, UP, DOWN
}

