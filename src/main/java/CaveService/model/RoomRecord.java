package CaveService.model;

import java.time.OffsetDateTime;
import java.time.ZonedDateTime;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * RoomRecord
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-04-14T15:40:41.753Z")

public class RoomRecord {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("creationTimeISO8601")
  private String creationTimeISO8601 = null;

  @JsonProperty("description")
  private String description = null;

  @JsonProperty("color")
  private String color = null;

  @JsonProperty("creatorId")
  private String creatorId = null;

  public RoomRecord id(String id) {
    this.id = id;
    return this;
  }

  public RoomRecord() {}

  public RoomRecord(String id, String description, String color, String creatorId) {
    this.description = description;
    this.color = color;
    this.creatorId = creatorId;
    this.creationTimeISO8601 = OffsetDateTime.now().toString();
    this.id = id;
  }

  // Josh Bloch - a copy constructor
  public RoomRecord(RoomRecord newRoom) {
    this.description = newRoom.getDescription();
    this.color = newRoom.getColor();
    this.creatorId = newRoom.getCreatorId();
    this.creationTimeISO8601 = newRoom.getCreationTimeISO8601();
    this.id = newRoom.getId();
  }


  /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(example = "(0,0,0)", value = "")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }


  /**
   * Get creationTimeISO8601
   * @return creationTimeISO8601
  **/
  @ApiModelProperty(example = "2020-04-13T15:04:43.084+02:00", value = "")

  @Valid

  public RoomRecord creationTimeISO8601(String creationTimeISO8601) {
    this.creationTimeISO8601 = creationTimeISO8601;
    return this;
  }

  public String getCreationTimeISO8601() {
    return creationTimeISO8601;
  }

  public void setCreationTimeISO8601(String creationTimeISO8601) {
    this.creationTimeISO8601 = creationTimeISO8601;
  }


  /**
   * Get description
   * @return description
  **/
  @ApiModelProperty(example = "You are standing at the end of a road before a small brick building.", value = "")

  public RoomRecord description(String description) {
    this.description = description;
    return this;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }


  /**
   * Get color
   * @return color
   **/
  @ApiModelProperty(example = "Yellow.", value = "")

  public RoomRecord color(String color) {
    this.color = color;
    return this;
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }


  /**
   * Get creatorId
   * @return creatorId
  **/
  @ApiModelProperty(example = "0", value = "")

  public RoomRecord creatorId(String creatorId) {
    this.creatorId = creatorId;
    return this;
  }

  public String getCreatorId() {
    return creatorId;
  }

  public void setCreatorId(String creatorId) {
    this.creatorId = creatorId;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RoomRecord roomRecord = (RoomRecord) o;
    return Objects.equals(this.id, roomRecord.id) &&
        Objects.equals(this.creationTimeISO8601, roomRecord.creationTimeISO8601) &&
        Objects.equals(this.description, roomRecord.description) &&
        Objects.equals(this.creatorId, roomRecord.creatorId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, creationTimeISO8601, description, creatorId);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RoomRecord {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    creationTimeISO8601: ").append(toIndentedString(creationTimeISO8601)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    creatorId: ").append(toIndentedString(creatorId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

