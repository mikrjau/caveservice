package CaveService.model.Events;

public class CreateRoomEventDTO extends EventDTO {
    String creatorId = null;
    String color = null;
    String description = null;
    String type = EventTypes.CreateRoomEvent;

    public String getCreatorId(){
        return creatorId;
    }
    public void setCreatorId(String creatorId){
        this.creatorId = creatorId;
    }

    public String getColor(){
        return color;
    }
    public void setColor(String color){
        this.color = color;
    }

    public String getDescription(){
        return description;
    }
    public void setDescription(String description){
        this.description = description;
    }

    public String getType(){
        return type;
    }
}
