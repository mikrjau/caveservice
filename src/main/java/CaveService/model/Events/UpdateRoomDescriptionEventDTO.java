package CaveService.model.Events;

public class UpdateRoomDescriptionEventDTO extends EventDTO {
    String description;
    String type = EventTypes.UpdateDescriptionEvent;

    public String getDescription(){
        return description;
    }
    public void setDescription(String description){
        this.description = description;
    }

    public String getType(){
        return type;
    }
}
