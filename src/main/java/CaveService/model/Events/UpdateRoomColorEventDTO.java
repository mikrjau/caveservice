package CaveService.model.Events;

import java.awt.*;

public class UpdateRoomColorEventDTO extends EventDTO {
    String color;
    String type = EventTypes.UpdateColorEvent;

    public String getColor(){
        return color;
    }
    public void setColor(String color){
        this.color = color;
    }

    public String getType(){
        return type;
    }
}
