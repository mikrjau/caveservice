package CaveService.model.Events;

public class EventTypes {
    public static final String CreateRoomEvent = "createRoom";
    public static final String UpdateColorEvent = "updateColor";
    public static final String UpdateDescriptionEvent = "updateDescription";
}
