package CaveService.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.Objects;

/**
 * HealthRecord
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-05-17T15:46:14.849Z")

public class HealthRecord   {
    @JsonProperty("title")
    private String title = null;

    @JsonProperty("state")
    private String state = null;

    @JsonProperty("requestTimeISO8601")
    private String requestTimeISO8601 = null;

    @JsonProperty("version")
    private String version = null;

    @JsonProperty("url")
    private String url = null;

    public HealthRecord() {}

    public HealthRecord(String state, String requestTimeISO8601) {
        this.title = "CaveService";
        this.state = state;
        this.requestTimeISO8601 = requestTimeISO8601;
        this.version = "v1";
        this.url = "http://msdo-oscar.cloud:5555/cave/health";
    }

    // Josh Bloch - a copy constructor
    public HealthRecord(HealthRecord newHealth) {
        this.title = newHealth.getTitle();
        this.state = newHealth.getState();
        this.requestTimeISO8601 = newHealth.getRequestTimeISO8601();
        this.version = newHealth.getVersion();
        this.url = newHealth.getUrl();
    }

    public HealthRecord title(String title) {
        this.title = title;
        return this;
    }

    /**
     * Get title
     * @return title
     **/
    @ApiModelProperty(example = "CaveService", value = "")

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public HealthRecord state(String state) {
        this.state = state;
        return this;
    }

    /**
     * Get state
     * @return state
     **/
    @ApiModelProperty(example = "healthy", value = "")

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }


    public HealthRecord requestTimeISO8601(String requestTimeISO8601) {
        this.requestTimeISO8601 = requestTimeISO8601;
        return this;
    }

    /**
     * Get requestTimeISO8601
     * @return requestTimeISO8601
     **/
    @ApiModelProperty(example = "2020-05-17T17:00:00.0000", value = "")

    @Valid

    public String getRequestTimeISO8601() {
        return requestTimeISO8601;
    }

    public void setRequestTimeISO8601(String requestTimeISO8601) {
        this.requestTimeISO8601 = requestTimeISO8601;
    }


    public HealthRecord version(String version) {
        this.version = version;
        return this;
    }

    /**
     * Get version
     * @return version
     **/
    @ApiModelProperty(example = "v1", value = "")

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }


    public HealthRecord url(String url) {
        this.url = url;
        return this;
    }

    /**
     * Get url
     * @return url
     **/
    @ApiModelProperty(example = "http://msdo-oscar.cloud:5555/cave/health", value = "")

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        HealthRecord healthRecord = (HealthRecord) o;
        return Objects.equals(this.title, healthRecord.title) &&
                Objects.equals(this.state, healthRecord.state) &&
                Objects.equals(this.requestTimeISO8601, healthRecord.requestTimeISO8601) &&
                Objects.equals(this.version, healthRecord.version) &&
                Objects.equals(this.url, healthRecord.url);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, state, requestTimeISO8601, version, url);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class HealthRecord {\n");

        sb.append("    title: ").append(toIndentedString(title)).append("\n");
        sb.append("    state: ").append(toIndentedString(state)).append("\n");
        sb.append("    requestTimeISO8601: ").append(toIndentedString(requestTimeISO8601)).append("\n");
        sb.append("    version: ").append(toIndentedString(version)).append("\n");
        sb.append("    url: ").append(toIndentedString(url)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

