package CaveService.model;

/**
 * ExitTypes
 */
public enum ExitType {
  NORTH,
  SOUTH,
  EAST,
  WEST,
  UP,
  DOWN
}
