package CaveService.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

public class UpdateRoomRecord {
    @JsonProperty("description")
    private String description = null;

    @JsonProperty("creatorId")
    private String creatorId = null;


    public UpdateRoomRecord description(String description) {
        this.description = description;
        return this;
    }

    /**
     * Get description
     * @return description
     **/
    @ApiModelProperty(example = "You are standing at the end of a road before a small brick building.", value = "")


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UpdateRoomRecord creatorId(String creatorId) {
        this.creatorId = creatorId;
        return this;
    }

    /**
     * Get creatorId
     * @return creatorId
     **/
    @ApiModelProperty(example = "0", value = "")


    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }


    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UpdateRoomRecord updateRoomRecord = (UpdateRoomRecord) o;
        return  Objects.equals(this.description, updateRoomRecord.description) &&
                Objects.equals(this.creatorId, updateRoomRecord.creatorId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description, creatorId);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class RoomRecord {\n");

        sb.append("    description: ").append(toIndentedString(description)).append("\n");
        sb.append("    creatorId: ").append(toIndentedString(creatorId)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

