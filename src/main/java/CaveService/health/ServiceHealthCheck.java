package CaveService.health;

import com.mongodb.MongoClientSettings;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import CaveService.model.HealthRecord;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class ServiceHealthCheck implements HealthCheck {
    private String host;
    private Integer port;

    public ServiceHealthCheck(String host, Integer port){
        this.host = host;
        this.port = port;
    }

    @Override
    public ResponseEntity<HealthRecord> geHealth() {
        HealthRecord healthRecord = null;
        List<ServerAddress> connections = Arrays.stream(this.host.split(",")).map(s -> new ServerAddress(s, this.port))
                .collect(Collectors.toList());

        try {
            MongoClient mongoClient = MongoClients.create(
                    MongoClientSettings.builder()
                            .applyToClusterSettings(builder -> builder.hosts(connections)
                            .serverSelectionTimeout(500, TimeUnit.MILLISECONDS))
                            .build());
            boolean dbExist = mongoClient.listDatabaseNames().into(new ArrayList<String>()).contains("cave");
            String state = (dbExist) ? "healthy" : "unhealthy";
            healthRecord = new HealthRecord(state, OffsetDateTime.now().toString());
        } catch (com.mongodb.MongoClientException e) {
            healthRecord = new HealthRecord("unhealthy", OffsetDateTime.now().toString());
        }

        HttpStatus httpStatus = (healthRecord.getState() == "healthy") ? HttpStatus.OK : HttpStatus.BAD_REQUEST;
        return new ResponseEntity<HealthRecord>(healthRecord,  httpStatus);
    }
}
