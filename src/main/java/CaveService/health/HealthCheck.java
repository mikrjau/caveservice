package CaveService.health;

import CaveService.model.HealthRecord;
import org.springframework.http.ResponseEntity;

public interface HealthCheck {
      ResponseEntity<HealthRecord> geHealth();
}
