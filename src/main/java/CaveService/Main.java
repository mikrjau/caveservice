package CaveService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@ComponentScan(basePackages = { "CaveService", "CaveService.api" , "CaveService.configuration", "CaveService.messaging"})
public class Main {
    public static void main(String[] args) throws Exception {
        new SpringApplication(Main.class)
            .run(args);
    }
}
