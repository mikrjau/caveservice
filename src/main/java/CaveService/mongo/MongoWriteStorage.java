package CaveService.mongo;

import CaveService.messaging.Publisher;
import CaveService.model.Events.CreateRoomEventDTO;
import CaveService.model.Events.UpdateRoomColorEventDTO;
import CaveService.model.Events.UpdateRoomDescriptionEventDTO;
import CaveService.model.ExitType;
import CaveService.model.Exits;
import CaveService.model.Point3;
import CaveService.model.RoomRecord;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClientSettings;
import com.mongodb.ServerAddress;
import com.mongodb.client.*;
import org.bson.Document;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static CaveService.model.Events.EventTypes.*;
import static com.mongodb.client.model.Filters.eq;

public class MongoWriteStorage implements RoomWriteStorage {
    private Publisher _publisher;
    private MongoCollection<Document> roomEventsCollection;
    private final String host;
    private final Integer port;

    public MongoWriteStorage(String host, Integer port, Publisher publisher) {
        this.host = host;
        this.port = port;
        _publisher = publisher;
    }

    @Override
    public void init() {
        List<ServerAddress> connections = Arrays.stream(this.host.split(",")).map(s -> new ServerAddress(s, this.port))
                .collect(Collectors.toList());

        System.out.println("Storage hosts list: " + connections.toString());

        MongoClient mongoClient = MongoClients.create(
                MongoClientSettings.builder()
                        .applyToClusterSettings(builder ->
                                builder.hosts(connections))
                        .build());
        MongoDatabase database = mongoClient.getDatabase("cave");
        roomEventsCollection = database.getCollection("roomEvents");
        createInitialRooms();
    }

    @Override
    public ResponseEntity<Void> createRoom(String positionString, RoomRecord newRoom) {
        CreateRoomEventDTO createRoomEvent = RoomRecordToCreateEvent(newRoom);
        return this.createRoomEvent(createRoomEvent.getPosition(), createRoomEvent);
    }

    public ResponseEntity<Void> createRoomEvent(String positionString, CreateRoomEventDTO createRoomEvent){
        Document roomEventDocument = roomEventsCollection.find(eq("position", positionString)).first();
        if (roomEventDocument != null && roomEventDocument.getString("type").equals(CreateRoomEvent))
            return new ResponseEntity<>(HttpStatus.CONFLICT);

        try {
            roomEventDocument = createRoomEventToDocument(positionString, createRoomEvent);
            roomEventsCollection.insertOne(roomEventDocument);
            _publisher.send(createRoomEvent);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Void> updateRoomColor(String positionString, String color) {
        UpdateRoomColorEventDTO updateRoomColorEvent = new UpdateRoomColorEventDTO();
        updateRoomColorEvent.setColor(color);
        return updateColorEvent(positionString, updateRoomColorEvent);
    }

    public ResponseEntity<Void> updateColorEvent(String positionString, UpdateRoomColorEventDTO updateRoomColorEvent){
        try {
            RoomRecord presentRecord = getRoomFromWriteModel(positionString).getBody();
            if (presentRecord == null)
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);

            updateRoomColorEvent.setTimestamp(OffsetDateTime.now().toString());
            updateRoomColorEvent.setPosition(positionString);
            Document roomEventDocument = updateColorEventToDocument(positionString, updateRoomColorEvent);
            roomEventsCollection.insertOne(roomEventDocument);
            _publisher.send(updateRoomColorEvent);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Void> updateRoomDescription(String positionString, String description) {
        UpdateRoomDescriptionEventDTO updateRoomDescriptionEvent = new UpdateRoomDescriptionEventDTO();
        updateRoomDescriptionEvent.setDescription(description);
        return updateDescriptionEvent(positionString, updateRoomDescriptionEvent);
    }

    public ResponseEntity<Void> updateDescriptionEvent(String positionString, UpdateRoomDescriptionEventDTO updateRoomDescriptionEvent){
        try {
            RoomRecord presentRecord = getRoomFromWriteModel(positionString).getBody();
            if (presentRecord == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

            updateRoomDescriptionEvent.setTimestamp(OffsetDateTime.now().toString());
            updateRoomDescriptionEvent.setPosition(positionString);
            Document roomEventDocument = updateDescriptionEventToDocument(positionString, updateRoomDescriptionEvent);
            roomEventsCollection.insertOne(roomEventDocument);
            _publisher.send(updateRoomDescriptionEvent);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Void> undoRoomEvent(String positionString) {
        RoomRecord presentRecord;

        try {
            presentRecord = getRoomFromWriteModel(positionString).getBody();
            if (presentRecord == null)
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);

            FindIterable<Document> reversedRoomEvents = roomEventsCollection
                    .find(eq("position", positionString))
                    .sort(new BasicDBObject("timestamp", -1));

            Document topEvent = reversedRoomEvents.first();
            if (topEvent == null)
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);

            String topType = topEvent.getString("type");
            if (topType.equals(CreateRoomEvent))
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

            reversedRoomEvents.skip(1);

            if (topType.equals(UpdateColorEvent)) {
                for (Document event : reversedRoomEvents) {
                    String innerEventType = event.getString("type");
                    if (topType.equals(innerEventType)) {
                        String color = event.getString("color");
                        updateRoomColor(positionString, color);
                        return new ResponseEntity<>(HttpStatus.CREATED);
                    }
                }
            }

            if (topType.equals(UpdateDescriptionEvent)) {
                for (Document event : reversedRoomEvents) {
                    String innerEventType = event.getString("type");
                    if (topType.equals(innerEventType)) {
                        String description = event.getString("description");
                        updateRoomDescription(positionString, description);
                        return new ResponseEntity<>(HttpStatus.CREATED);
                    }
                }
            }

            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<Exits> getExits(String positionString) {
        Exits listOfExits = new Exits();
        RoomRecord room;

        try {
            room = getRoomFromWriteModel(positionString).getBody();

            if (room == null)
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);

            Point3 pZero = Point3.parseString(positionString);
            Point3 p;
            for (ExitType d : ExitType.values()) {
                p = new Point3(pZero.x(), pZero.y(), pZero.z());
                p.translate(d);
                String position = p.getPositionString();
                ResponseEntity<RoomRecord> potentialExitRoom = getRoomFromWriteModel(position);
                if (potentialExitRoom.getBody() != null)
                    listOfExits.add(d);
            }
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(listOfExits, HttpStatus.OK);
    }


    private void createInitialRooms() {
        this.createRoom("(0,0,0)", new RoomRecord("(0,0,0)", "You are standing at the end of a road before a small brick building.", "black", WILL_CROWTHER_ID));
        this.createRoom("(0,1,0)", new RoomRecord("(0,1,0)", "You are in open forest, with a deep valley to one side.", "white", WILL_CROWTHER_ID));
        this.createRoom("(1,0,0)", new RoomRecord("(1,0,0)", "You are inside a building, a well house for a large spring.", "red", WILL_CROWTHER_ID));
        this.createRoom("(-1,0,0)", new RoomRecord("(-1,0,0)", "You have walked up a hill, still in the forest.", "yellow", WILL_CROWTHER_ID));
        this.createRoom("(0,0,1)", new RoomRecord("(0,0,1)", "You are in the top of a tall tree, at the end of a road.", "green", WILL_CROWTHER_ID));
    }

    private ResponseEntity<RoomRecord> getRoomFromWriteModel(String positionString) {
        FindIterable<Document> roomEvents = roomEventsCollection
                .find(eq("position", positionString))
                .sort(new BasicDBObject("timestamp", 1));
        Document roomCreateEvent = roomEvents.first();
        if (roomCreateEvent == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        if (!roomCreateEvent.getString("type").equals(CreateRoomEvent))
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

        RoomRecord baseRoomRecord = createEventToRoom(roomCreateEvent);
        /*
        FindIterable<Document> iterable = roomEvents.skip(1);
        for (Document eventDocument: iterable) {
            String type = eventDocument.getString("type");
            switch (type) {
                case UpdateColorEvent:
                    baseRoomRecord.setColor(eventDocument.getString("color"));
                    break;
                case UpdateDescriptionEvent:
                    baseRoomRecord.setDescription(eventDocument.getString("description"));
                    break;
                default:
                    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        */

        return new ResponseEntity<>(baseRoomRecord, HttpStatus.OK);
    }

    private Document createRoomEventToDocument(String positionString, CreateRoomEventDTO createRoomEvent){
        return new Document()
                .append("position", positionString)
                .append("timestamp", createRoomEvent.getTimestamp())
                .append("type", createRoomEvent.getType())
                .append("creatorId", createRoomEvent.getCreatorId())
                .append("color", createRoomEvent.getColor())
                .append("description", createRoomEvent.getDescription());
    }

    private CreateRoomEventDTO RoomRecordToCreateEvent(RoomRecord room) {
        String position = room.getId();
        String timestamp = room.getCreationTimeISO8601();
        String description = room.getDescription();
        String color = room.getColor();
        String creatorId = room.getCreatorId();

        CreateRoomEventDTO createEvent = new CreateRoomEventDTO();
        createEvent.setPosition(position);
        createEvent.setTimestamp(timestamp);
        createEvent.setDescription(description);
        createEvent.setColor(color);
        createEvent.setCreatorId(creatorId);
        return createEvent;
    }

    private RoomRecord createEventToRoom(Document document) {
        String position = document.getString("position");
        String timestamp = document.getString("timestamp");
        String description = document.getString("description");
        String color = document.getString("color");
        String creatorId = document.getString("creatorId");

        RoomRecord roomRecord = new RoomRecord(position, description, color, creatorId);
        roomRecord.setCreationTimeISO8601(OffsetDateTime.parse(timestamp).toString());
        return roomRecord;
    }

    private RoomRecord documentToRoom(Document document) {
        String position = document.getString("position");
        String timestamp = document.getString("timestamp");
        String description = document.getString("description");
        String color = document.getString("color");
        String creatorId = document.getString("creatorId");

        RoomRecord roomRecord = new RoomRecord(position, description, color, creatorId);
        roomRecord.setCreationTimeISO8601(OffsetDateTime.parse(timestamp).toString());
        return roomRecord;
    }

    private Document updateColorEventToDocument(String positionString, UpdateRoomColorEventDTO updateRoomColorEvent) {
        return new Document()
                .append("position", positionString)
                .append("timestamp", updateRoomColorEvent.getTimestamp())
                .append("type", updateRoomColorEvent.getType())
                .append("color", updateRoomColorEvent.getColor());
    }

    private Document updateDescriptionEventToDocument(String positionString, UpdateRoomDescriptionEventDTO updateRoomDescriptionEvent) {
        return new Document()
                .append("position", positionString)
                .append("timestamp", updateRoomDescriptionEvent.getTimestamp())
                .append("type", updateRoomDescriptionEvent.getType())
                .append("description", updateRoomDescriptionEvent.getDescription());
    }

}
