package CaveService.mongo;

import CaveService.model.RoomRecord;
import com.mongodb.MongoClientSettings;
import com.mongodb.ServerAddress;
import com.mongodb.client.*;
import org.bson.Document;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.mongodb.client.model.Filters.eq;

public class MongoReadStorage implements RoomReadStorage {
    private MongoCollection<Document> roomsCollection;
    private final String host;
    private final Integer port;

    public MongoReadStorage(String host, Integer port) {
        this.host = host;
        this.port = port;
    }

    @Override
    public void init() {
        List<ServerAddress> connections = Arrays.stream(this.host.split(",")).map(s -> new ServerAddress(s, this.port))
                .collect(Collectors.toList());
        System.out.println("Storage hosts list: " + connections.toString());

        MongoClient mongoClient = MongoClients.create(
                MongoClientSettings.builder()
                        .applyToClusterSettings(builder ->
                                builder.hosts(connections))
                        .build());
        MongoDatabase database = mongoClient.getDatabase("cave");
        roomsCollection = database.getCollection("rooms");
    }

    @Override
    public ResponseEntity<RoomRecord> getRoom(String positionString) {
        RoomRecord roomRecord = null;
        Document roomDocument = roomsCollection.find(eq("position", positionString)).first();
        if(roomDocument == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        } else {
            try {
                roomRecord = documentToRoom(roomDocument);
            } catch (Exception ex) {
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }
        }

        return new ResponseEntity<RoomRecord>(roomRecord, HttpStatus.OK);
       }
    private RoomRecord documentToRoom(Document document) {
        String position = document.getString("position");
        String timestamp = document.getString("timestamp");
        String description = document.getString("description");
        String color = document.getString("color");
        String creatorId = document.getString("creatorId");

        RoomRecord roomRecord = new RoomRecord(position, description, color, creatorId);
        roomRecord.setCreationTimeISO8601(OffsetDateTime.parse(timestamp).toString());
        return roomRecord;
    }
}
