package CaveService.mongo;

import CaveService.model.RoomRecord;
import org.springframework.http.ResponseEntity;

public interface RoomReadStorage {
    void init();

    ResponseEntity<RoomRecord> getRoom(String positionString);
}
