package CaveService.mongo;

import CaveService.model.*;
import org.springframework.http.ResponseEntity;

public interface RoomWriteStorage {
  String WILL_CROWTHER_ID = "0";

  void init();

  ResponseEntity<Void> createRoom(String positionString, RoomRecord newRoom);

  ResponseEntity<Void> updateRoomDescription(String positionString, String description);

  ResponseEntity<Void> updateRoomColor(String positionString, String color);

  ResponseEntity<Void> undoRoomEvent(String positionString);

  ResponseEntity<Exits> getExits(String positionString);
}
