# Server
CaveService based on the Spring Boot REST API, using embedded Tomcat server for hosting.

# How to run
1. Start your server as an simple java application (run the Swagger2SprintBoot class) 
2. You can view the api documentation in swagger-ui by pointing to http://localhost:5555/cave/swagger-ui.html