# Use to update image:
#   docker build -t msdooscar/skycave:caveservice . && docker push msdooscar/skycave:caveservice

# Build step
FROM maven:3.6.0-jdk-8-slim AS build
LABEL MAINTAINER = 'Group Oscar'

WORKDIR /home
COPY src ./src
COPY pom.xml .

RUN mvn -f . clean package

# Server step
FROM openjdk:8-slim as server

WORKDIR /home
COPY --from=build home/target/CaveService-LATEST.jar .

# Add curl package
RUN apt-get update && apt-get install curl -y

EXPOSE 5555
CMD ["java","-jar","CaveService-LATEST.jar"]